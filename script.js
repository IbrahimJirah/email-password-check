//create fictional email
let userEmail = 'example@gmail.net';
//create fictional password
let userPassword = '1234';

//create userEmail verification function
function emailCheck (email){
  if((email.includes('@')) && (email.includes('.com'))){
    return true;
  }
  else if((email.includes('@')) && (email.includes('.co.uk'))){
    return true;
  }
  else if((email.includes('@')) && (email.includes('.org'))){
    return true;
  }
  else if((email.includes('@')) && (email.includes('.net'))){
    return true;
  }
  else {
    return false;
  }
}
// console.log(userCheck(userEmail));

//create userpassword verification function
function passwordCheck(passWord){
  if(passWord.includes('_') && passWord.includes('!')){
    return true;
  }
  else {
    return false;
  }
}
console.log(passwordCheck(userPassword));

